<?php

require_once(dirname(__FILE__)."/mgvo_sniplets.php");
 
class MGVO_GEN_SNIPLET extends MGVO_SNIPLET {
 
 // *************************
	  // Generic Sniplets
	  //
	  //  Mit diesen Funktion lassen sich generisch beliebige HTML-Tabellen aus MGVO erstellen. 
	  //  Mit den Einträgen $vkal_use_fields_table, $vkal_head_fields_tablen und $vkal_sanitize_fields_table  wird gesteuert, welche Felder ausgegeben werden
      //  Die verfügbaren Felder können dem XML (<objfieldlist>) entnommen werden und sind auch im Array der API verfügbar)
      //  $vkal_use_fields_table =  zu verwendendene Felder  
      //  $vkal_head_fields_table = dazugehörige Überschriften (gleiche Anzahl wie Felder erforderlich)
      //  $vkal_sanitize_fields_table = Felder, die eine Datumsbehandlung benötigen
	  //  $vkal_sort gibt den sortfeldnamen vor. Übergabe als String (mit Komma) oder als Array
	  //  $vkal_filter gibt Filterkritieren vor. Übergabe als Feld=String mit Komma ("ortid=saal1,startzeit=20:00:00") oder als Array (array(array('field'=>'ortid','value'=>'saal1'),array('field'=>'startzeit','value'=>'20:00:00'), 
	  //  Nur im Array werten Leer- und Sonderzeichen unterstützt. Aktuell wir nur das "=" unterstützt. (kine "<" oder "!=")
	  //  $vkal_rewrite_fildes Hiermit können Felder in andere Felder geschrieben werden (wenn diese leer sind) z.B. resstarttime in startzeit für Ortsreservierungen
	  //  Im MGVO Kalender werden, je nach Eintragsursprng eine Vielzahl unterschiedlicher Felder gefüllt, daher ist dies in manchen Fällen notwendig.
	  //  $max_count - Die maxminale Anzahl von Einträgen je Seite
	  //  $page - Die Seite (nur in Verbindung mit $maxcount)
	  //  Die Spalten haben jeweils eine CSS-Klasse mgvo-f-<feldname>, z.B. mgvo-f-bez, somit lassen sich im CSS die Spaltenbreiten individuell angeben.
	  //  Wenn im Namen "NOWEB" steht, wird der Eintrag niemals ausgegeben
	  //  Wenn im Namen EXTERN: steht, wird nur der Teil nach EXTERN: ausgegeben. Praktisch um z.B. Extern die Details auszulassen "Saal vermietet an Hans Schmidt EXTERN:Private Veranstaltung"
	  //
	  // *************************


	  function mgvo_gen_sniplet_gruppen ( $vkal_use_fields_table = Null, $vkal_head_fields_table = Null, $vkal_sort = NULL, $vkal_filter = NULL, $vkal_rewrite_fields = NULL, $max_count = NULL, $page= NULL) {
         // Liest die Gruppen ein und gibt sie aus
         //  Verfügbare Felder: 
		 // Konfig-Anleitung siehe oben
		 if (empty($vkal_use_fields_table) or empty($vkal_head_fields_table)) {
			// Alle Felder mit technischem Namen
            // $vkal_use_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
            // $vkal_head_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
			// Default:
			$vkal_use_fields_table = explode(",","grubez,grutxt,startzeit,endzeit,turnus,ortid,tridall");
			$vkal_head_fields_table = explode(",","Gruppenname,Beschreibung,Start,Ende,Turnus,Ort,Trainer");
        }
        if (count($vkal_use_fields_table) != count($vkal_head_fields_table)) {
            $sniplet .= "Anzahl der Felder und Überschriften in mgvo_sniplet_vkal() nicht gleich";
			return $sniplet;
        }      
        $vkal_sanitize_fields_table  = explode(",","startdat,startzeit,enddat,endzeit");
 
        // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
        $resar = $this->api->read_gruppen($vkalnr,$seljahr);
		return $this->mgvo_generic_sniplet($resar , "mgvo-vkal" ,$vkal_use_fields_table, $vkal_head_fields_table, $vkal_sanitize_fields_table, $vkal_sort, $vkal_filter, $max_count, $page);
	  }	
      
      /*unction mgvo_gen_sniplet_gruppen_bgc ( $vkal_use_fields_table = Null, $vkal_head_fields_table = Null, $vkal_sort = NULL, $vkal_filter = NULL, $vkal_rewrite_fields = NULL, $max_count = NULL, $page= NULL) {
         // Liest die Gruppen ein und gibt sie aus
         //  Verfügbare Felder: 
		 // Konfig-Anleitung siehe oben
		 if (empty($vkal_use_fields_table) or empty($vkal_head_fields_table)) {
			// Alle Felder mit technischem Namen
            // $vkal_use_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
            // $vkal_head_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
			// Default:
			$vkal_use_fields_table = explode(",","grubez,grutxt,startzeit,endzeit,ortid,tridall");
			$vkal_head_fields_table = explode(",","Gruppenname,Beschreibung,Start,Ende,Ort,Trainer");
            //$vkal_filter= "gzfld03=Herbst2021";
        }
        if (count($vkal_use_fields_table) != count($vkal_head_fields_table)) {
            $sniplet .= "Anzahl der Felder und Überschriften in mgvo_sniplet_vkal() nicht gleich";
			return $sniplet;
        }      
        $vkal_sanitize_fields_table  = explode(",","startdat,startzeit,enddat,endzeit");

        // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
        $resar = $this->api->read_gruppen($vkalnr,$seljahr);
		return $this->mgvo_generic_sniplet($resar , "mgvo-vkal" ,$vkal_use_fields_table, $vkal_head_fields_table, $vkal_sanitize_fields_table, $vkal_rewrite_fields, $vkal_sort, $vkal_filter, $max_count, $page);
	  }	*/ 
      
	  
	  function mgvo_gen_sniplet_vkal($vkalnr,$seljahr, $vkal_use_fields_table = Null, $vkal_head_fields_table = Null, $vkal_sort = NULL, $vkal_filter = NULL, $vkal_rewrite_fields = NULL, $max_count = NULL, $page= NULL) {
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr als HTML. 
         // Verfügbare Felder: startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb
		 // ortid,eventnr,ressdat,resstime,resedat,resetime,ebez,eltxt,eort,startdat,startzeit,enddat,endzeit,email_org,betr01,betrbez01,ticketbis,saalplan,publish,pub_helfer,resmaildat,helfermaildat,abrechdat,pubdate,url_eventinfo,pm_bar,teilnehmerflag,emtickflag,ort,ortkb,bez,prio,stdabrech,bgcol,ticketmail,verkaufsintro,vorldat,wartelisteaktiv,notiz,betr02,betrbez02,porto,sonderueber,pm_ueber,pm_ls
		 // Konfig-Anleitung siehe oben
		 if (empty($vkal_use_fields_table) or empty($vkal_head_fields_table)) {
			// Alle Felder mit technischem Namen
            // $vkal_use_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
            //$vkal_head_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
			// Default:
			$vkal_use_fields_table = explode(",","startdat,startzeit, bez,ort");
			$vkal_head_fields_table = explode(",","Datum,Start,Veranstaltungen, ort");
        }
        if (count($vkal_use_fields_table) != count($vkal_head_fields_table)) {
            $sniplet .= "Anzahl der Felder und Überschriften in mgvo_sniplet_vkal() nicht gleich";
			return $sniplet;
        }  
		if (empty($vkal_rewrite_fields)) {
			$vkal_rewrite_fields= array(  'resstime' => 'starzeit', 'resetime' => 'endzeit', 'ressdat' => 'startdat');
		}
		
        $vkal_sanitize_fields_table  = explode(",","startdat,startzeit,enddat,endzeit");
 
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_vkal($vkalnr,$seljahr);
	 
		 return $this->mgvo_generic_sniplet($resar , "mgvo-vkal" ,$vkal_use_fields_table, $vkal_head_fields_table, $vkal_sanitize_fields_table, $vkal_rewrite_fields, $vkal_sort, $vkal_filter, $max_count, $page);
	  }
	  
	  
      function mgvo_generic_sniplet($resar, $css_class, $use_fields_table, $head_fields_table, $sanitize_fields_table, $rewrite, $sort, $filter, $max_count, $page) {
         
		 $work_array = 	$resar['objar'];
         //mgvo_add_index( $work_array) ; 
		 
		 $sniplet = "<div class='mgvo ".$css_class."'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table cellpadding=1 cellspacing=0 border=1>";
         $sniplet .= "<tr>";
         
		 // Sortierwerte können entweder als String übergeben werden (mit Komma getrent) oder als Array
		 if($sort != NULL && !is_array($sort)) {
			$sort = explode(",",$sort);
		 }
 		 
		 // Filter können entweder als String mit "feld=Wert,feld=wert,...' übergeben werden (mit Komma getrent) oder als Array. Leerzeichen im Vergleichswert, Sonderzeichen, etc. werden nur im Array unterstützt. 
		 // Groß/Klein wird ignoriert, es wird bisher nur "=" unterstützt. (Kein !=, etc.)
		 /*if($filter != NULL && !is_array($filter))  {
			$filterset = explode(",",$filter);  // "bez,Ball,strpos" oder "ortid,Saal1,="
			$filter = [];
			 foreach($filtersets as $id => $filterset) {
				$filters = explode('=', $filterset);
				if (count($filters) != 2) {error_log("MGVO:mgvo_generic_sniplet: nicht korrekte Filterkritieren".print_r($filtersets)); continue;}  
				$filter[$id]['field'] = trim($filters[0]);
				$filter[$id]['value'] = trim($filters[1]);
				$filter['operator'] = 'EQ'; // For future use
			 } 
		 } */
		 
		 
		 // Sichern der index (damit nach dem Sortieren die URLs für Einzelelemente noch passen) - noch nicht klar, ob benötigt, kommt auf das sort an. Mit uasort sollte das so gehen
		 //foreach($resar['objar'] as $idx => $vkr) {
		 //	 $vkr['index'] = $idx;
		 //}
		 	 
		 // mgvo_filter_array( $resar, $filterfield, $value, $operator = "==", $and = False) 
		 if ($filter != Null) {
			$work_array = $this->mgvo_filter_array( $work_array, $filter['field'], $filter['value'], $filter['operator'], filter['modus']);	
		 }
		 
		 if($sort != NULL){ 
			 foreach($sort as $sortfield) {
				 uasort ($work_array, function ($a, $b) {if ($a == $b) {return 0;} return ($a[$sortfild] < $b[sortfield]) ? -1 : 1;} );
			 } 
		 }
		 
         foreach ($head_fields_table  as  $header) {
                $sniplet .= "<th class='mgvo-h-".$header."'>".$header."</th>";
         }
         $sniplet .= "</tr>";
		 
		 $max_count=1;
		 //print_r($vkr);
         foreach($work_array as $idx => $vkr) {
			// Sind filterkriterien aktiv, prüfen, ob diese zutreffen, sonst weiter.
            print_r($vkr);
            echo "Schleife vor filter";
			if($filter != NULL) {
				foreach($filter as $filter2) {
					if (!$vkr[$filter2['Field']] == $filter2['value'] ) { continue; }
				}
			}
			if ( $count != NULL &&  // keine Anzahl angegeben
					( !($count > $max_count && $page = NULL)  // keine Seite angeben, aktuelle Anzahl größer Max_Count
					  || ! (($count * $page <= $maxcount) && !(($count * ($page+1)) <=  $maxcount))  // nicht auf der aktuellen Seite 
					)
				) { 
				$count++ ; continue; 
			}
			
            
			if (strstr($vkr['bez'],"NO_WEB")) {continue; } 
					
			if (  strstr($vkr['bez'] , "EXTERN:" )) {
					$vkr['bez'] = str_replace ("EXTERN:","",strstr($vkr['bez'],"EXTERN:")); // Titel ab Extern: (ohne Extern:)
			}
			
			if(stristr(vkr['bez'], "!") != false)  {
				$vkr['bez'] =stristr(vkr['bez'], "!", true);
            }               // alles nach ! entfernen
			
			// Rewrite
			if ( $rewrite != NULL) {
				foreach ($rewrite as $source => $dest) {
				    if ($vkr[$dest]="") {
						$vkr[$dest] = $vkr[$source];
					}
				}
			}
			
            $sniplet .= "<tr>";
            foreach ($use_fields_table as  $field) {
                if (isset($vkr[$field])) {
                    if (in_array($field, $sanitize_fields_table)) {
                        $sniplet .= "<td class='mgvo-f-".$field."'>".date2user($vkr[$field])."</td>"; 
                    } else {
                        $sniplet .= "<td class='mgvo-f-".$field."'>".$vkr[$field]."</td>"; 
                    }
                } else {
                   $sniplet .= "<td class='mgvo-f-".$field."'></td>";
				}
            }
            $sniplet .= "</tr>"; 
			$count++;
         }
         $sniplet .= "</table>";
         $sniplet .= "</div>";
         return $sniplet;
      }
    

	
	  function mgvo_gen_sniplet_vkal_entry($vkalnr,$seljahr, $arrayindex, $use_fields_table = Null, $head_fields_table = Null, $vkal_rewrite_fields = NULL) {
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr und gibt den Temrin mit der EventID aus. 
         //  Verfügbare Felder: startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb
		 // Konfig-Anleitung siehe oben
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
            //$use_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
			//$head_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
            $use_fields_table = explode(",","startdat,startzeit,endzeit,bez,ortid");
			$head_fields_table = explode(",","Datum,Start,Ende,Veranstaltung,Ort");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und Überschriften in mgvo_sniplet_vkal_entry() nicht gleich";
			return $sniplet;
        }  
		if (empty($vkal_rewrite_fields)) { // Überschreiben von Feldern
			$vkal_rewrite_fields= array('starzeit' => 'resstime', 'endzeit' => 'resetime', 'startdat' => 'ressdat');
		}
		
        //$sanitize_fields_table  = explode(",","startdat"); 
 
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_vkal($vkalnr,$seljahr);
		 
		 return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-vkal-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, $vkal_rewrite_fields );
	  }
	
    
    function mgvo_gen_sniplet_vkal_entry2($vkalnr,$seljahr, $arrayindex, $use_fields_table = Null, $head_fields_table = Null, $vkal_rewrite_fields = NULL) {
         // BGC-Spezialfunktion (aus zwei funktionen zusammen)
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
            //$use_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
			//$head_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
            $use_fields_table = explode(",","startdat,startzeit,endzeit,bez,ort");
			$head_fields_table = explode(",","Datum,Start,Ende,Veranstaltung,Ort");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und Überschriften in mgvo_sniplet_vkal_entry() nicht gleich";
			return $sniplet;
        }  
		if (empty($vkal_rewrite_fields)) { // Überschreiben von Feldern
			$vkal_rewrite_fields= array('starzeit' => 'resstime', 'endzeit' => 'resetime', 'startdat' => 'ressdat');
		}
	     $sanitize_fields_table  = explode(",","startdat"); 
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_vkal($vkalnr,$seljahr);
		 
		 //return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-vkal-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, $vkal_rewrite_fields );
         
         // function mgvo_generic_sniplet_entry($resar, $arrayindex, $css_class, $use_fields_table, $head_fields_table, $sanitize_fields_table, $rewrite ) {
         $css_class =  "mgvo-vkal-entry";        
         $rewrite = $vkal_rewrite_fields; 
         //error_log("Resarray:");
         //error_log(print_r($resar, true));    
             
         $sniplet = "<div class='mgvo ".$css_class."'>";
         //$sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table cellpadding=1 cellspacing=0 border=1>";
		 
		 //error_log("mgvo_generic_sniplet_entry: arrayindex:".$arrayindex." ccs ".$css_class." use_field ".print_r($use_fields_table, true)." head ".print_r($head_fields_table, true));
         
		 $fields_table = array_combine($use_fields_table, $head_fields_table);
		 //error_log("gen_entry:".substr (print_r($resar, true),0,2500));
		 
		 // Rewrite fehlt noch
		 
		 $sniplet .= "<tr>";
		 foreach ($fields_table  as  $field => $header) {
			$sniplet .= "<tr><th class='mgvo-h-".$field."'>".$header."</th>";
			
			if(!empty($rewrite) &&  in_array( $field,$rewrite)) {
				$resar['objar'][$arrayindex][$field] = $resar['objar'][$arrayindex][$rewrite[$field]];
			}
			
			if (!empty($sanitize_fields_table) && in_array($field, $sanitize_fields_table)) {
				$sniplet .= "<td class='mgvo-f-".$field."'>".date2user($resar['objar'][$arrayindex][$field])."</td></tr>"; 
			} else {
                switch ($field) {
                    case "startzeit":
                    case "endzeit":
                        $sniplet .= "<td class='mgvo-f-".$field."'>".substr($resar['objar'][$arrayindex][$field],0,5)." Uhr</td></tr>";
                        break;
                    case "startdat":
                        $sniplet .= "<td class='mgvo-f-".$field."'>".date2user($resar['objar'][$arrayindex][$field])."</td></tr>"; 
                        break;
                    default:
				        $sniplet .= "<td class='mgvo-f-".$field."'>".$resar['objar'][$arrayindex][$field]."</td></tr>"; 
                }
			}		
		 }
         
         $groupid = $resar['objar'][$arrayindex]['gruid'];
         
         $resar = $this->api->read_gruppen();
         $work_array = 	$resar['objar'];
         
         foreach($work_array as $idx => $vkr) {
             if ($vkr['gruid'] == $groupid) {
                 
                 $sniplet .= "<tr><th class='mgvo-h-grutxt'>Beschreibung</th>";
                 $sniplet .= "<td >".$vkr['grutxt']."</td></tr>";
                 break;
             }
         }
         
         
         
         $sniplet .= "</table>";
         $sniplet .= "</div>";
         return $sniplet;
      
	  }
    
		function mgvo_gen_sniplet_gruppen_entry($arrayindex = '', $gruid = '', $use_fields_table = Null, $head_fields_table = Null) {
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr und gibt den Temrin mit der EventID aus. 
         //  Verfügbare Felder: startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb
		 // Konfig-Anleitung siehe oben
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
			// $use_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
            // $head_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
		    $use_fields_table = explode(",","grubez,wotag,startzeit,endzeit,ortid,grutxt,trnameall");
			$head_fields_table = explode(",","Gruppe,Tag,Start,Ende,Ort,Beschreibung,Trainer");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und Überschriften in mgvo_sniplet_gruppen_entry() nicht gleich";
			return $sniplet;
        }  
		
        $sanitize_fields_table  = explode(",","startdat,enddat");
		 
         // Liest die Gruppen
         $resar = $this->api->read_gruppen();
         if ($arrayindex =='' ) { // dann wohl eine GruppenID 
            $work_array = $resar['objar'];
            foreach($work_array as $idx => $vkr) {
                //error_log('test gruid:'.$vkr['gruid']);
                if ($vkr['gruid'] == $gruid) {
                    $arrayindex = $idx;
                    //error_log('idx found');
                    break;
                }
            }
         }
		 
		 return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-gruppen-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, NULL );
	  }
      
      
      
      
	
	  function mgvo_gen_sniplet_event_entry($arrayindex, $use_fields_table = Null, $head_fields_table = Null) {
         // Liest den Veranstaltungseintrag mit Nr. 
         //  Verfügbare Felder: eventnr,name,ort,startdate,starttime,enddate,endtime,bgcol,email_org,publish,resmaildat,helfermaildat,abrechdat,pubdate,description,vorldat,pub_helfer,wartelisteaktiv,notiz,betr01,betrbez01,ticketbis,saalplan,link,pm_bar,teilnehmerflag,emtickflag,besturl
		 // Konfig-Anleitung siehe oben
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
			// $use_fields_table = explode(",","eventnr,name,ort,startdate,starttime,enddate,endtime,bgcol,email_org,publish,resmaildat,helfermaildat,abrechdat,pubdate,description,vorldat,pub_helfer,wartelisteaktiv,notiz,betr01,betrbez01,ticketbis,saalplan,link,pm_bar,teilnehmerflag,emtickflag,besturl");
            // $head_fields_table = explode(",","eventnr,name,ort,startdate,starttime,enddate,endtime,bgcol,email_org,publish,resmaildat,helfermaildat,abrechdat,pubdate,description,vorldat,pub_helfer,wartelisteaktiv,notiz,betr01,betrbez01,ticketbis,saalplan,link,pm_bar,teilnehmerflag,emtickflag,besturl");
		    $use_fields_table = explode(",","startdate,starttime,name,ort");
			$head_fields_table = explode(",","Datum,Uhrzeit,Veranstaltung,Ort");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und Überschriften in mgvo_sniplet_vkal_entry() nicht gleich";
			return $sniplet;
        }  
		
        $vkal_sanitize_fields_table  = explode(",","starttime,endtime");
 
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_events();
		 
		 return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-event-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, NULL );
	  }
	
	  function mgvo_gen_sniplet_notraining_entry($arrayindex, $use_fields_table = Null, $head_fields_table = Null) {
         // Liest den Veranstaltungseintrag mit Nr. 
         //  Verfügbare Felder: resdat,starttime,endtime,neustarttime,neuendtime,gruid,abtbez,normbelegung,ortsbez,reservierungsgrund
		 // Konfig-Anleitung siehe oben
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
			// $use_fields_table = explode(",","resdat,starttime,endtime,neustarttime,neuendtime,gruid,abtbez,normbelegung,ortsbez,reservierungsgrund");
            // $head_fields_table = explode(",","resdat,starttime,endtime,neustarttime,neuendtime,gruid,abtbez,normbelegung,ortsbez,reservierungsgrund");
		    $use_fields_table = explode(",","resdat,starttime,gruid,ortsbez,reservierungsgrund ");
			$head_fields_table = explode(",","Datum,Uhrzeit,Gruppe,Ort,Grund");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und Überschriften in mgvo_sniplet_notraining_entry() nicht gleich";
			return $sniplet;
        }  
		
        $vkal_sanitize_fields_table  = explode(",","starttime,endtime");
 
         // Liest die Trainingsausfälle
         $resar = $this->api->read_training_fail();
		 
		 return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-notraining-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, NULL );
	  }
	
	
	  function mgvo_generic_sniplet_entry($resar, $arrayindex, $css_class, $use_fields_table, $head_fields_table, $sanitize_fields_table, $rewrite ) {
             
         //error_log("Resarray:");
         //error_log(print_r($resar, true));    
             
         $sniplet = "<div class='mgvo ".$css_class."'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table cellpadding=1 cellspacing=0 border=1>";
		 
		 //error_log("mgvo_generic_sniplet_entry: arrayindex:".$arrayindex." ccs ".$css_class." use_field ".print_r($use_fields_table, true)." head ".print_r($head_fields_table, true));
         
		 $fields_table = array_combine($use_fields_table, $head_fields_table);
		 //error_log("gen_entry:".substr (print_r($resar, true),0,2500));
		 
		 // Rewrite fehlt noch

 	 
		 $sniplet .= "<tr>";
		 foreach ($fields_table  as  $field => $header) {
			$sniplet .= "<tr><th class='mgvo-h-".$field."'>".$header."</th>";
			
			if(!empty($rewrite) &&  in_array( $field,$rewrite)) {
				$resar['objar'][$arrayindex][$field] = $resar['objar'][$arrayindex][$rewrite[$field]];
			}
			
			//error_log("gen_snpilet_entry: field:".$field." val:".$resar['objar'][$arrayindex][$field]);
			if (($field == 'grubez') && (  stristr($resar['objar'][$arrayindex][$field],"!") != false )  ) {
				$resar['objar'][$arrayindex][$field] = stristr($resar['objar'][$arrayindex][$field],"!", true); 
				//error_log("gen_snpilet_entry: CHANGE  !!!:".$field." val:".$resar['objar'][$arrayindex][$field]);
			}
			
			
			if (!empty($sanitize_fields_table) && in_array($field, $sanitize_fields_table)) {
				$sniplet .= "<td class='mgvo-f-".$field."'>".date2user($resar['objar'][$arrayindex][$field])."</td></tr>"; 
			} else {
				$sniplet .= "<td class='mgvo-f-".$field."'>".$resar['objar'][$arrayindex][$field]."</td></tr>"; 
			}		
		 }
         $sniplet .= "</table>";
         $sniplet .= "</div>";
         return $sniplet;
      } 
	  
	  /*
	  public function sanitize_date($d){
		if ($d) {  // yyyy-mm-dd
		  return date2user(datefield);
		} else {
			if ($d match ) { // 
				return (subst($d, 0, 6);
			} else {
				return $d;
			}
		}			
	  }*/
	  
	  /* 
	  * Manipulationsfunktionen
	  * Diese können auf ein mehrdimensionales Array, so wie sie die Funktion create_ergar()['objar'] aus der API (d.h. alle Read-Funktionen 
	  * der API verwendert werden.
	  * Beispiel: 
	  * $result_array = read_gruppen();
	  * $obj_array = result_array['objar'];
	  * mgvo_add_index(&$obj_array); // Übergabe als Referenz
	  * $filtered_array = mgvo_filter_array( $ojb_array, 'grubez',"ball", "stripos"); // Ergibt alle  Gruppen die ein "ball" oder "Ball" enthalten.
	  *
	  */ 
	  
	  function mgvo_filter_array( $resar, $filterfield, $value, $operator = "==", $and = False) {  
		  // Filter ein Array nach einem Feldwert, Rückgabe eines Array, 
		  // $resar - übergebenen array 
		  // $filterfield - Feld, auf das gefiltert wird
		  // $value - Wert mit dem verglichen wird
		  // $operator - Vergleichsoperator, aktuell nur "==", "!=" "<", ">" und strpos bzw. stripos unterstützt.
		  // die filter, values und operatoren können auch Arrays sein, wenn dann aber alle als Array
		  // $and ob, bei mehrenen Vergleichen ein AND (true) oder OR (false) angewendet wird
		  // Achtung, die Funktion behält den Index nixht bei.
		  $new_ar = array();
		  foreach($resar as $idx => $entry) { 
			  if(is_array($filterfield)) {
				    $and ? $result = true : $result = false;
					foreach ($filterfield as $filter_idx => $field) {
						  if ( mgvo_compare($entry[$field],$value[$idx], $operator[$ix])) {
							  if (!$and) $result = true; // bei OR reicht ein einziger positiver Vergleich
						  } else {
							  if ($and) $result = false; // bei AND ein einiger Negativer
						  }
					}
					if ($result) $new_ar[] = $entry;
			  } else {
					if ( $this->mgvo_compare($entry[$filterfield],$value, $operator)) {
						$new_ar[] = $entry;
					}
			  }
		  }
		  return $new_ar;	
	  
	  }
	  function mgvo_compare($a, $b, $operator) {
		  // Hilfsfunktion für mgvo_filter_array
		  // Vergleich $a mit $b mit den angegebenen Operator
		  switch ($operator) {
			  case "==": return ($a == $b);
			  case "!=": return ($a != $b);
			  case "<" : return ($a < $b);
			  case ">" : return ($a > $b);
			  case "strpos": return (strpos($a, $b) !== false);
			  case "stripos": return (stripos($a, $b) !== false);
		  }
      }
		  

			  
	  
	  function mgvo_add_index( &$resar) {
	  // die Funktion fügt jedem Hauptelement einen schlüssel "idx" mit dem Wert des aktuellen index hinzu.
	  // praktisch, wenn die nachfolgenden Funktionen das Array bzw. den Index ggf. verändern (z.B. mgvo_filter_array())
	  // wichtig ist dies für alle Sniplets und Wordpressfunktionen, die Links/URLs auf Einzelnwerte referenzieren. Hier muss der Index exakt mit dem Orinalindex übereinstimmen. 
	  // Die Keys aus dem XML sind leider nicht immer zur Zuordnung verwendbar (z.B. erhalten mehrere Ortsreservierungen alle den selben Veranstaltungs-Key)
	  
		  foreach($resar as $idx => $entry) {
			  $entry['idx']= $idx;
		  }
		  return;  
	  }
	  
		function mgvo_merge_fields(&$resar, $from__field, $to_field, $force = FALSE) {
		  // kopiert Feldwerte in andere Felder (wenn diese leer sind)
		  // wenn es der Schlüssel noch nicht angelegt ist, wird dieser erzeugt
		  // existiert der Schlüssel und der Wert Wert ist ungleich "" wird dieser nicht überschrieben, außer $force = TRUE
		  // $from__field, $to_field, können jeweils Einzelwerte als String oder Arrays sein
		  // Praktisch ist die Funktion um Kalenderevents aus unterschiedlichen Qellen zusammenzufassen. Diese haben sehr z.T. sehr unterschiedliche Felder
		  // die hiermit bei Bedarf in einheitliche zusammengefasst werden können. (Eine Veranstaltung hat z.B. eine Start und ein Enddatum, eine Feiertag oder Traingsausfall aber nur genau ein "Datum")
		  foreach($resar as $idx => $entry) {
			  if(is_array($from__field)) {
					foreach ($from__field as $from_idx => $field) {
						  if (empty($entry[$to_field[$from_idx]]) or $force==true) {
								$entry[$to_field[$from_idx]] = $entry[$field];
						  }
					}
			  } else {
					if(empty($entry[$to_field])) {
						$entry[$to_field] = $entry[$from_field];
					}
			  }
			
			}
			return;
		}
	
	  
   }
   