<?php


require_once(dirname(__FILE__)."/mgvo_sniplets.php");

$resource_table = array ();
 
class MGVO_FULLCALENDAR_SNIPLET extends MGVO_SNIPLET {
	
	 // *************************
	  // Generic Sniplets
	  //
	  //  Mit diesen Funktion lassen sich Kalendaransichten basierden auf der JavaScript-Bibliothek fullcalendar.io beliebige 
	  //HTML-Tabellen aus MGVO erstellen. 
	  // *************************

	  
	 public function mgvo_gen_resource_table (){
		 // Die Funktion erstellt eine Resourcentabelle mit allen Räumen.
		 // Dazu werden die Orte aus MGVO gelesen
		 // Hierbei kann ein zentreales Konfigfile herangezogen werden
		 // Output ist ein Array mit
		 //   "ortbez" Ortname lang (aus MGVO)
		 //	  "ortid" Orts ID (aus MGVO)
		 //   "ortkb" (Ort Kurzbezeichnung) (aus MGVO)
		 //	  "ortmatch" Regulärer Ausdruck, der wenn ein Raum angegeben wurde, der nicht den ersten dreien entspricht, verwendet werden. (aus Konfigfile)
		 //   Beispiel: Bei einer Veranstaltung wir als Ort (der dort Freitext ist) "Saal Wiesbaden" angegeben. Mit /*.Wiesbaden*./ wird dies der korrekten Bezeichung "Raum Wiesbaden" zugeordnet.
		 //	  "ortcal" Raumname Kalenderspalte (aus Konfigdatei, sonst = ortkb)
		 //	  "ortlist" Raumname für die Liste (aus Konfigdatei, sonst = ortkb)
		 //	  "ortlink" Link auf eine Raum bzw. Anfahrtsbeschreibung. (aus Konfig, sonst leer)
		 // Die Tabelle wird für die Ortszuordnung sowie für die Ortbeschreibungen und Verlinkungen benötigt.
		 // Hinweis: in MGVO wird eine Trainingsstätte mit "Ort" bezeichnet, in Fulcalender mit "Ressource", an anderen stellen mit "Room" alles ist aber identisch. Dies führt schnell zu Misverständnissen.
		 // die Resourcetable wird auch in einer globlaen Variable bereitgestellt.
		 
		 // Später sollten diese Setting in der WP-Oberfläche sein
		 $settings_resources = array ( 
		   	 "BSort" => array ('ortsmatch' => "/*.Bespiel*./", 'ortscal' => "Beispl." , 'ortlink' => "http://beispiellink.de")
		 );
		 
		 global $resource_table;
		 
         $resar = $this->api->read_orte();
		 $resource_table = array();
         foreach($resar['objar'] as $or) {
			if (isset($or['ortid']) ) { 
				$resource_table[$or['ortid']] = array ( 
					 'ortid' =>  $or['ortid'], 
					 'ortbez' => isset($or['ortbez']) ? $or['ortbez'] : $or['ortid'],  
					 'ortkb' => isset($or['ortkb']) ? $or['ortkb'] : $or['ortid'] ,
					 'ortmatch' => isset($settings_resources[$or['ortid']][ortmatch]) ? $settings_resources[$or['ortid']][ortmatch] :"",
					 'ortscal' => isset($settings_resources[$or['ortid']][ortscal]) ?$settings_resources[$or['ortid']][ortscal] :$or['ortkb'],
					 'ortlist' => isset($settings_resources[$or['ortid']][ortlist]) ? $settings_resources[$or['ortid']][ortlist] :$or['ortkb'],
					 'ortlink' => isset($settings_resources[$or['ortid']][ortlink]) ? $settings_resources[$or['ortid']][ortlink] :"",
					);
            } else {
				mgvo_log("Ortselement ohne ID aus MGVO gelesen",$or,MGVO_DEBUG_ERR, __FUNCTION__);
			}
         }
         return $resource_table;
 
	 }
	 
	 public function sanitize_resource ($ort, $type = "ortid", $strikt = FALSE) {
		// versucht die passenden Resourceneintrag zu $ort finden.
		// Als Rückgabe wird der Wert aus dem Resourcearray mit dem Schlüssel "$type" (ortid, ortkb, etc.) zurückgegeben. 
		// Alternativ liefert $type = "array" das komplette Array für diesen Eintrag zurück. 
		// Mit $strikt = True muss $ort entweder ortid, ortkb oder ortbez exakt übereinstimmen. 
		// Mit $strikt = False werden leerzeichen am Anfang und Ende entfernt sowie alles Case-Insensitive verglichen. 
		// Kommt es zu keiner übereinstimmung, wird versucht mit ortmatch ein Matching zu erreichen.
		// Mit einem Match = "/.*/ kann auch ein Defaultraum bestimmt werden.
		// wird nichts gefunden, wird bei stric=False der Ort unverändert zurückgegeben
		
		global $resource_table; 
		
		// 		
		if (isset ($resource_table[$ort])) {
				return $this->return_resource($resource_table[$ort], $type, $ort, $strikt);
				
		}
		
	 }
	 
	 public function return_resource($resource_table_entry, $type, $ort, $strikt ) {
		 // Hilffunktion, gibt den geforderterten Eintrag zurück.
		 if ($type == "array") {
			 return $resource_table_entry;
		 } else {
			 if (isset($resource_table_entry[$type])) {
				 return $resource_table_entry[$type];
			 } else {
				 if ($strikt) {
					 return "";
					 mgvo_log("Ein Ort konte nicht aufgelöst werden, Resourcearray unvollständig",$ort,MGVO_DEBUG_ERR, __FUNCTION__);
				 } else {
					 return $ort;
				 }
			 }
		 }
	 }
		 
}